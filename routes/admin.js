module.exports = function(app, plugin_router, plugins) {
  var express = require('express');

  var router = express.Router();

  var multer = require('multer')
  var upload = multer({
    dest: 'uploads/'
  })

  router.get('/endpoints', function(req, res, next) {
    var endpoints = [];
    for (var i = 0; i < plugin_router.stack.length; i++) {
      var endpoint = plugin_router.stack[i];
      endpoints.push({
        path: endpoint.route.path,
        method: endpoint.route.stack[0].method
      });
    };
    res.send(endpoints);
  });

  router.get('/plugins', function(req, res, next) {
    res.send(plugins);
  });

  return router;
};