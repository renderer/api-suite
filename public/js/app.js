var app = angular.module('apisuite', []);

app.controller('EndpointController', function($scope, $http){
  $scope.endpoints = [];
  $scope.path = '';
  $scope.test = {};

  $scope.setCurrentEndpoint = function(endpoint) {
    $scope.test = {};
    $scope.test.hash = "dc93891f6773cbeeb7871c6ba7c22a19";
    $scope.test.path = endpoint.path;
    $scope.endpoint = endpoint;
    delete $scope.result;
  };
  
  $http.get('/admin/endpoints').then(function(res) {
    $scope.endpoints = res.data;
    $scope.setCurrentEndpoint($scope.endpoints[0]);
  });


  $scope.submitTest = function() {
    var path = $scope.test.path;
    var method = $scope.endpoint.method;
    var data = $scope.test.data;
    var hash = $scope.test.hash;
    if(hash != '') {
      path += "?secure_hash=" + hash;
    }

    $http({
      method: method,
      url: path,
      data: data
    }).then(function(result) {
      $scope.result = result;
    }, function(result) {
      $scope.result = result;
    });
  };
});

app.controller('PluginController', function($scope, $http) {
  $scope.plugins = {};
  $scope.current = 0;

  $http.get('/admin/plugins').then(function(res) {
    $scope.plugins = res.data;
    for(var i in $scope.plugins) {
      $scope.current = i;
      break;
    }
  });

  $scope.show = function(pid) {
    $scope.current = pid;
  }
});