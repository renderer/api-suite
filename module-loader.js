var winston = require('winston');

module.exports = function(systemServices) {
  var pluginsInfo = {};
  var pluginsToLoad = require('./plugins');
  var serviceCaches = {};

  if(systemServices) {
    pluginsInfo['system'] = {
      name: "System",
      description: "provide core API"
    };
    var serviceNames = [];
    for(var serviceName in systemServices) {
      serviceCaches[serviceName] = systemServices[serviceName];
      serviceNames.push(serviceName);
    }
    if(serviceNames.length > 0) {
      pluginsInfo['system'].services = serviceNames.join(', ');
    }
  }

  (function loadNext() {
    if (!pluginsToLoad || pluginsToLoad.length == 0) {
      return;
    }
    var pluginName = pluginsToLoad.shift();
    var plugin = require(pluginName);


    var reqNames = plugin.requirements || [];

    pluginsInfo[pluginName] = {
      name: plugin.name,
      description: plugin.description
    }
    var requirementsString = "NONE";
    if(reqNames.length > 0) {
      pluginsInfo[pluginName].requirements = reqNames.join(', ');
    }


    var permNames = plugin.permissions || [];
    if(permNames.length > 0) {
      pluginsInfo[pluginName].permissions = permNames.join(', ');
    }

    var reqs = [];

    for (var i = 0; i < reqNames.length; i++) {
      var reqName = reqNames[i];
      reqs.push(serviceCaches[reqName]);
    };

    reqs.push(function(services) {
      winston.info("Plugin " + pluginName + " loaded!");
      if (!services) {
        return loadNext();
      }
      var serviceNames = [];
      for (var serviceName in services) {
        serviceCaches[serviceName] = services[serviceName];
        serviceNames.push(serviceName);
      }
      if(serviceNames.length > 0) {
        pluginsInfo[pluginName].services = serviceNames.join(", ");
      }
      loadNext();
    });

    if (!plugin.callback) {
      loadNext();
    }

    plugin.callback.apply({}, reqs);

  })();

  return pluginsInfo;
};