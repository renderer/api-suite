module.exports = function(app, User, checkPerm) {

  /**
   * @api {get} /users get users list
   * @apiVersion 0.2.0
   * @apiName GetUsers
   * @apiGroup User
   *
   * @apiSuccess [User] array of users
   *
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     [{
   *       "firstname": "John",
   *       "lastname": "Doe"
   *     }, ...]
  */
  app.get('/users', checkPerm('users.list'), function(req, res, next) {
    User.findAll().then(function(users) {
      res.send(users);
    }).catch(next);
  });

  /**
   * @api {get} /users/:id get user information
   * @apiVersion 0.2.0
   * @apiName GetUser
   * @apiGroup User
   *
   * @apiParam {Number} id Users unique ID.
   *
   * @apiSuccess {String} username  Firstname of the User.
   * @apiSuccess {String} password   Lastname of the User.
   *
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "firstname": "John",
   *       "lastname": "Doe"
   *     }
  */
  app.get('/users/:id', function(req, res, next) {
    User.findOne({
      where: {
        id: parseInt(req.params.id)
      }
    }).then(function(user) {
      res.send(user);
    }).catch(next);
  })

  /**
   * @api {post} /users add a new user
   * @apiSampleRequest http://localhost:3000/users
   * @apiVersion 0.2.0
   * @apiName AddUser
   * @apiGroup User
   *
   * @apiParam {User} user User data
   *
   * @apiSuccess {String} username  Firstname of the User.
   * @apiSuccess {String} password   Lastname of the User.
   *
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "firstname": "John",
   *       "lastname": "Doe"
   *     }
  */
  app.post('/users', checkPerm('users.add'), function(req, res, next) {
    User.create(req.body).then(function(user) {
      res.send(user);
    }).catch(next);
  });
};