module.exports = {
  name: "User Service",
  description: "expose RESTful API for users",
  requirements: ['router', 'model.User', 'checkPerm'],
  permissions: ['users.list', 'users.add', 'users.delete', 'users.update'],
  callback: function(app, User, checkPerm, register) {
    require('./user-service')(app, User, checkPerm);
    register();
  }
};