module.exports = {
  name: "Sequelize Sync",
  description: "sync sequelize models when loaded",
  requirements: ['sequelize'],
  callback: function(sequelize, register) {
    sequelize.sync().then(function() {
      register();
    });
  }
};