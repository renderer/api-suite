module.exports = {
  name: "User Model",
  description: "provide a model for User table",
  requirements: ['sequelize'],
  callback: function(sequelize, register) {
    var User = sequelize.import('./user.js');
    register({
      'model.User': User
    });
  }
};