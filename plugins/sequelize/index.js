var Sequelize = require('sequelize');
var sequelize = new Sequelize('test', 'root', '');

module.exports = {
  name: "Sequelize",
  description: "provide a preconfigured sequelize instance",
  requirements: [],
  callback: function(register) {
    register({
      'sequelize': sequelize
    });
  }
};