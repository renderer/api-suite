var data = {
  roles: [ 'admin', 'guest' ],
  keys: {
    "dc93891f6773cbeeb7871c6ba7c22a19": "admin",
    "5742ab1bf701b1502b7b4c2ec7befdc9": "guest"
  },
  mappings: {
    "users.list": ["admin", "guest"],
    "users.add": ["admin"]
  }
};

module.exports = {
  checkAuthInfo: function(req, cb) {
    var hash = req.query.secure_hash;
    if(!hash) {
      return cb();
    }
    var role = data.keys[hash];
    if(!role) {
      return cb();
    }
    req.role = role;
    cb();
  },
  checkPermission: function(req, permissionName, cb) {
    var roles = data.mappings[permissionName];
    var role = req.role;
    console.log(role);
    if(roles.indexOf(role) != -1) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  }
};